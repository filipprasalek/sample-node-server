const getenv = require('getenv');

module.exports = {
  port: getenv.int('SERVER_PORT', 3000),
  greeting: getenv('GREETING', 'Sample greeting!')
}