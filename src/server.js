const express = require('express');
const health = require('./api/health');
const config = require('./config');

const app = express();

app.get('/', (req, res) => {
  res.send('Hello World! ' + config.greeting);
});

app.use('/api/health', health);

app.get('/api/users', (req, res) => {
  res.json({ "status": "ok" });
});

app.get('/api/version', (req, res) => {
  res.json({ version: config.greeting })
});

module.exports = app;