const supertest = require('supertest');
const app = require("../../src/server");

describe('/health route tests', () => {
  it('should return 200 when app is healthy', done => {
      supertest(app).get('/api/health').expect(200).end(() => done());
  });

  afterAll(() => {
    app.close();
  })
});