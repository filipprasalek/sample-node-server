const supertest = require('supertest');
const app = require("../src/server");

describe('app base test', () => {
  it('it should return 200 when home endpoint is called', done => {
      supertest(app).get('/').expect(200).end(() => done());
  });

  afterAll(() => {
    app.close();
  })
});