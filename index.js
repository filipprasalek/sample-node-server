const server = require('./src/server');
const config = require('./src/config')

server.listen(config.port, () => {
  console.log(`Sample node server listening at http://localhost:${config.port}`)
});

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Server has been shutdown...');
  });
});